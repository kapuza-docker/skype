FROM ubuntu:latest
MAINTAINER Kaka Puzikova <gitlab@tost.info.tm>

ENV DEBIAN_FRONTEND noninteractive

RUN useradd -m skype

RUN apt update
RUN apt install -y --no-install-recommends ca-certificates \
	apt-transport-https \
	curl

RUN echo "deb https://repo.skype.com/deb stable main" > /etc/apt/sources.list.d/skype-stable.list
RUN curl https://repo.skype.com/data/SKYPE-GPG-KEY | apt-key add -
RUN apt update
RUN apt install -y --no-install-recommends ca-certificates \
	skypeforlinux

RUN rm -rf /var/lib/apt/lists/*
RUN rm -f /var/cache/apt/archives/*.deb

COPY entrypoint.sh /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
